let mongoose = require("mongoose");
let Schema = mongoose.Schema;

// La définition d'un Schema :

let BookSchema = new Schema({
  title: { type: String, required: true },
  author: { type: String, required: true },
  year: { type: Number, required: true },
  pages: { type: Number, required: true, min: 1 },
  CreatedAt: { type: Date, default: Date.now }
},
{
    versionKey: false
}
);

// CreatedAt défini avec les paramètres de la date courante
BookSchema.pre('save', next => {
    now = new Date();
    if(!this.CreatedAt) {
        this.CreatedAt = now
    }
    next()
});

//Export du Schema pour l'utiliser où on veut
const Book = mongoose.model('Book', BookSchema);

module.exports = Book;

