process.env.NODE_ENV = 'test';

let mongoose = require('mongoose');
let Book = require('../models/book');

// Les dépendances pour le développement

/*
Avoir chai
Avoir chai http
Avoir should
*/

chai = require('chai')
chaiHttp = require('chai-http')
server = require('../app')
should = chai.should();

chai.use(chaiHttp);

describe('Books', () => {
    beforeEach((done) => { // Avant chaque test tu vas vider la database
        Book.remove({}, (err) => {
            done()
        })
    })

    describe('/GET book', () => {
        it('should GET return all books', (done) => {
            chai.request(server)
                .get('/book')
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('array');
                    res.body.length.should.be.eql(0);
                    done();
                })
        })
    })

    describe('/POST book', () => {
        it('shoud not POST a book without pages fields', (done) => {
            let book = {
                title: 'The Lord of the Rings',
                author: 'J.R.R. Tolkien',
                year: 1954
            }
            chai.request(server)
                .post('/book')
                .send(book)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('errors');
                    res.body.errors.should.have.property('pages');
                    res.body.errors.pages.should.have.property('kind').eql("required");
                    done();
                });
        });

        it('should POST a book', (done) => {
            let book = {
                title: 'The Lord of the Rings',
                author: 'J.R.R. Tolkien',
                year: 1954,
                pages: 1170
            }
            chai.request(server)
                .post('/book')
                .send(book)
                .end((err, res) => {
                    res.should.have.status(200);
                    res.body.should.be.a('object');
                    res.body.should.have.property('message').eql("Book successfully added!");
                    res.body.book.should.have.property('title');
                    res.body.book.should.have.property('author');
                    res.body.book.should.have.property('pages');
                    res.body.book.should.have.property('year');
                    done();
                })
        })
    })

    describe('/GET/:id book', () => {
        it('should GET a book by the given id', (done) => {
            let book = new Book({
                title: "The Lord of the Rings",
                author: "J.R.R. Tolkien",
                year: 1954,
                pages: 1170
            })
            book.save((err, book) => {
                chai.request(server)
                    .get(`/book/${book.id}`)
                    .send(book)
                    .end((err,res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('title');
                        res.body.should.have.property('author');
                        res.body.should.have.property('pages');
                        res.body.should.have.property('year');
                        res.body.should.have.property('_id').eql(book.id);
                        done();
                    })
            })
        })
    })

    describe('/PUT/:id', () => {
        it('should UPDATE a book given the id', (done) => {
            let book = new Book({title: "The Chronicles of Narnia",author: "C.S Lewis",year: 1948,pages: 778})
            book.save((err, book) => {
                chai.request(server)
                    .put(`/book/${book.id}`)
                    .send({title: "The Chronicles of Narnia", author: "C.S Lewis", year: 1950, pages: 778})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Book updated!');
                        res.body.book.should.have.property('year').eql(1950);
                        done();
                    })
            })
        })
    })

    describe('/DELETE/:id book', () => {
        it('should DELETE a book by the given id', (done) => {
            let book = new Book({
                title: "The Lord of the Rings",
                author: "J.R.R. Tolkien",
                year: 1954,
                pages: 1170
            })
            book.delete((err, book) => {
                chai.request(server)
                    .delete(`/book/${book.id}`)
                    .send(book)
                    .end((err,res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('Book successfully deleted');
                        // res.body.book.should.have.property('ok').eql(1);
                        // res.body.book.should.have.property('n').eql(1);
                        done();
                    })
            })
        })
    })
});
