
### Book-node
--
C'est un petit projet en `node.js`
avec les outils (libs) annexes afin de mieux comprendre l'architecture d'une application avec node.js.

TODO :

- [x] Le sever se lance (express)
- [x] Définir une/des route(s)
- [x] Faire un model (mongoose)
- Test avec mocha & chai
    -[x] GET all books
    -[x] POST a book
    -[x] GET/:id a book
    -[x] PUT/:id a book
    -[x] DELETE/:id a book
- [x] Connecter à la base de donnée (mongoDB)
- [x] Tester avec Insomnia/Postman (RESTfull tools)
- [x] Install template engine
- [x] Render page with template engine