const express = require('express');
const app = express();
const config = require('config');
const mongoose = require('mongoose');
const volleyball = require('volleyball');
const book = require('./routes/book');

app.use(express.static(__dirname+'/public'));
app.set("view engine","pug");
app.set("views","./views");

// let options = {
//     server: {socketOptions: {keepAlive:1, connectTimeoutMS:30000} },
//     repLqet: {socketOptions: {keepAlive:1, connectTimeoutMS:30000} },
// };
mongoose.connect(config.DBHost);
let db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));
db.once('open', () => {
    console.log([`MongoDB is ready`]);
});
if (config.util.getEnv("NODE_ENV") !== 'test') {
    app.use(volleyball);
}
app.use(express.json())
app.use(express.urlencoded({
    extended: true
}));

const port = config.PORT;

app.get('/', (req, res) => {
    res.render('home');
})

app.route('/book')
    .get(book.getBooks)
    .post(book.postBook)
app.route('/book/new')
    .get(book.getForm)
app.route('/book/:id')
    .get(book.getBook)
app.route('/book/:id/edit')
    .get(book.getEditForm)
    .post(book.updateBook)
app.route('/book/:id/delete')
    .get(book.deleteBook)



app.listen(port, () => console.log(`[App is on fire on:${port}]`));
module.exports = app; //for testing

// RESTful => CRUD (Create / Read / Update / Delete)
// GET ==> READ ===> localhost:5000/book/all
// POST ==> CREATE ===> localhost:5000/book/new